/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ergasia3;

/**
 *
 * @author ktistak
 */
public class HeartsPlayer extends Human implements CardsPlayer {

    private String nickname;
    private Card[] hand = new Card[5];

//    constructors
    public HeartsPlayer() {
        this.nickname = null;
    }

    public HeartsPlayer(String name, String surname, int age, String nickname) {  //xrisimopoiw ton constructor tis yperklasis

        super(name, surname, age);
        this.nickname = nickname;
    }


//    getters
    public String getNickname() {
        return nickname;
    }

    public Card[] getHand() {
        return hand;
    }

//  setters
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setHand(Card[] hand) {
        this.hand = hand;
    }

//    methods
    public void showHand() {    //tipwno ton pinaka pou antiproswpeuei to xeri tou paikti
        System.out.println("\n\nTa fylla tou " + this.getNickname()+":\n");
        for (int i = 0; i < hand.length; i++) {
            System.out.print(" " + this.hand[i].toString());
//            System.out.print(" "+ this.hand[i].getSymbol()+this.hand[i].getNumber());    //an den eixa tin toString
        }
        System.out.println("\n\n");
    }

    @Override
    public void introduceSelf() {   //O paiktis systeinetai, dld ektypwnw ola ta stoixeia tou!
        System.out.println("Hi!My name is " + this.getName() + " " + this.getSurname() + " and I`m " + this.getAge() + " years old!");
        if (this.getNickname() == null) {
            System.out.println("I love to play Hearts but I don`t have a nickname!");
        } else {
            System.out.println("I love to play Hearts and I`m known as " + this.getNickname());

        }
    }

    public int GetNumOfHearts()
    {
        int hearts = 0;
        for (int i = 0; i < hand.length; i++) {
            if (hand[i].getSymbol().equals("#")) {
                hearts++;
            }
        }
        return hearts;
    }
}
