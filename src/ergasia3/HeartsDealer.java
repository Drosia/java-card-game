/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ergasia3;

/**
 *
 * @author ktistak
 */
public class HeartsDealer extends Human implements CardsDealer {

    private int yearsAsDealer;              //kathe dealer exei xronia proypiresias kai mia trapoula
    private Deck dealersDeck = new Deck();  //edw to field einai ena deck..den ylopoioume apo tin arxi enan pinaka apo card!h klassh deck ton exei mesa!

//    constructors
    public HeartsDealer() {
        this.yearsAsDealer = 0;
    }

    public HeartsDealer(String name, String surname, int age, int yearsAsDealer) {
        super(name, surname, age);
        this.yearsAsDealer = yearsAsDealer;
    }

//    getters
    public int getYearsAsDealer() {
        return yearsAsDealer;
    }

    public Deck getDealersDeck() {
        return dealersDeck;
    }

//    setters
    public void setYearsAsDealer(int yearsAsDealer) {
        this.yearsAsDealer = yearsAsDealer;
    }

    public void setDealersDeck(Deck dealersDeck) {
        this.dealersDeck = dealersDeck;
    }

//    methods
    @Override
    public void introduceSelf() {   //o dealer systeinetai, dld ektypwnw ola ta stoixeia tou!
        System.out.println("Hi!I`m " + this.getAge() + " and my name is " + this.getName() + " " + this.getSurname());
        if (this.getYearsAsDealer() < 3) {
            System.out.println("I `m new in the job but don`t worry!");
        } else {
            System.out.println("I` m doing this job for " + this.getYearsAsDealer() + " years!");

        }
    }

    public void showDeck() {    //typwnw olo ton pinaka tou deck me eksipno tropo gia na fainetai wraia
        int p = 0;
        System.out.println("Parakalw deite oti i trapoula exei ola ta fylla:");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 13; j++) {
                Card currentCard = this.dealersDeck.getDeckCards()[p++]; //apothikeuw se mia metavliti tin karta tis trexousas thesis
                System.out.print(" " + currentCard.toString());       //an den eixa tin toString tha xrisimopoiousa  currentCard.getSymbol()+currentCard.getNumber() stin thesi tis

            }
            System.out.println("");     //afinw mia grammi gia na fainetai wraia stin typwsi

        }
    }

    public Card dealRandomCard() {
        Card selectedCard;
        int x;
        do {
            x = (int) (Math.random() * 52);
            selectedCard = this.dealersDeck.getDeckCards()[x];    //apothikeuoume tin karta pou einai se mia tyxaia thesi se mia metavliti
        } while (selectedCard == null);                           //an einai null epanalamvanoume tin diadikasia

        this.dealersDeck.getDeckCards()[x] = null;            //otan vroume mia pou den einai null,dld pou den exei dwthei tote tin epistrefoume afou kanoume prin null tin thesi tou pinaka pou itan auti i karta

        return selectedCard;
    }

    public void dealToPlayers(Object player1, Object player2) {     //moirazei apo to deck tou 5 fylla se kathe paikti.
        HeartsPlayer pl1 = (HeartsPlayer) player1;                  //me typecast to metatrepoume se HeartsPlayer gia na exoume prosvasi sto field Hand
        HeartsPlayer pl2 = (HeartsPlayer) player2;

        for (int i = 0; i < pl1.getHand().length; i++) {
            pl1.getHand()[i] = dealRandomCard();                    //gemizoume ton pinaka hand twn paiktwn me mia tyxaia karta se kathe thesi
        }

        for (int i = 0; i < pl2.getHand().length; i++) {
            pl2.getHand()[i] = dealRandomCard();
        }

    }

    public void decideWinner(Object player1, Object player2) {      //apofasizei poios exei perissoteres koupes apo tous 2 paiktes
        HeartsPlayer pl1 = (HeartsPlayer) player1;                  //me typecast to metatrepoume se HeartsPlayer gia na exoume prosvasi sto field Hand
        HeartsPlayer pl2 = (HeartsPlayer) player2;

        int hearts1 = 0;
        int hearts2 = 0;

        for (int i = 0; i < pl1.getHand().length; i++) {            //an h karta stin thesi i exei symvolo # tote einai koupa kai auksanoume ton antistoixo metriti
            if (pl1.getHand()[i].getSymbol().equals("#")) {
                hearts1++;
            }
            if (pl2.getHand()[i].getSymbol().equals("#")) {
                hearts2++;
            }
        }

        String tmp;
        if (hearts1 > hearts2) {                        //me aplous elegxous kanoume omorfes typwseis gia to apotelesma tou paixnidiou

            if (hearts1 == 1) {
                tmp = "koupa";
            } else {
                tmp = "koupes";
            }
            System.out.println("Nikitis einai o " + pl1.getNickname() + " me " + hearts1 + " " + tmp);
        } else if (hearts1 < hearts2) {
            if (hearts2 == 1) {
                tmp = "koupa";
            } else {
                tmp = "koupes";
            }
            System.out.println("Nikitis einai o " + pl2.getNickname() + " me " + hearts2 + " " + tmp);
        } else {
            if (hearts2 == 1) {
                tmp = "koupa";
            } else {
                tmp = "koupes";
            }
            System.out.println("Exoume isopalia me "+hearts2+ " "+ tmp);
        }

    }
}
