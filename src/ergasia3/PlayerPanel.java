/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ergasia3;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Gktistakis
 */
public class PlayerPanel extends JPanel {

    HeartsPlayer player;
    JPanel panelHand;
    JButton butIntroduce;
    JButton butShowHand;
    JLabel labPoints;
    JLabel labHearts;
    JLabel labTitle;

    public PlayerPanel(HeartsPlayer thePlayer) {


        this.player = thePlayer;
        JPanel panelLeft = new JPanel(new GridLayout(2, 1));
        JPanel panelCenter = new JPanel();
        JPanel panelRight = new JPanel(new GridLayout(2, 1));
        this.add(panelLeft);
        this.add(panelCenter);
        this.add(panelRight);

        panelHand = panelCenter;

        JPanel panelNorthLeftTop = new JPanel();
        JPanel panelNorthLeftBottom = new JPanel();

        panelLeft.add(panelNorthLeftTop);
        panelLeft.add(panelNorthLeftBottom);

        labTitle = new JLabel(player.getNickname());

        panelNorthLeftTop.add(labTitle);


        butIntroduce = new JButton("introduce");
        butShowHand = new JButton("show hand");

        panelNorthLeftBottom.add(butIntroduce);
        panelNorthLeftBottom.add(butShowHand);

        labHearts = new JLabel("Number of Hearts: 0");
        labPoints = new JLabel("Total Points: 0");

        panelRight.add(labHearts);
        panelRight.add(labPoints);


        for (int i = 0; i < 5; i++) {
            panelHand.add(new EmptySpot());

        }
    }

    public HeartsPlayer getPlayer() {
        return player;
    }

    public JPanel getPanelHand() {
        return panelHand;
    }

    public JButton getButIntroduce() {
        return butIntroduce;
    }

    public JButton getButShowHand() {
        return butShowHand;
    }

    public JLabel getLabTitle() {
        return labTitle;
    }

    public JLabel getLabHearts() {
        return labHearts;
    }

    public JLabel getLabPoints() {
        return labPoints;
    }

    public void setPlayer(HeartsPlayer player) {
        this.player = player;
    }

    
}
