/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ergasia3;

import java.awt.event.ActionEvent;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 *
 * @author Gktistakis
 */
public class DealerPanel extends JPanel {

    private HeartsDealer dealer;
    private JButton butIntruduce;
    private JButton butShowDeck;
    private JButton butDeal;
    private JButton butDecide;

    public DealerPanel(HeartsDealer theDealer) {
        this.dealer = theDealer;

        this.setLayout(new GridLayout(3, 1));
        JPanel panelTop = new JPanel();
        JPanel panelCenter = new JPanel(new GridLayout(1, 2));
        JPanel panelBottom = new JPanel(new GridLayout(1, 2));

        this.add(panelTop);
        this.add(panelCenter);
        this.add(panelBottom);

        JLabel label_dl = new JLabel("Dealer");
        panelTop.add(label_dl);

        butIntruduce = new JButton("introduce");
        butShowDeck = new JButton("show deck");
        panelCenter.add(butIntruduce);
        panelCenter.add(butShowDeck);

        butDeal = new JButton("deal");
        
        butDecide = new JButton("decide winner");
        panelBottom.add(butDeal);
        panelBottom.add(butDecide);
    }

    public HeartsDealer getDealer() {
        return dealer;
    }

    public JButton getButIntruduce() {
        return butIntruduce;
    }

    public JButton getButShowDeck() {
        return butShowDeck;
    }

    public JButton getButDeal() {
        return butDeal;
    }

    public JButton getButDecide() {
        return butDecide;
    }

    public void setDealer(HeartsDealer dealer) {
        this.dealer = dealer;
    }


}
