/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ergasia3;

import java.awt.event.ActionEvent;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 *
 * @author Gktistakis
 */
public class PockerTable extends JFrame {

    

    HeartsDealer theDealer;
    HeartsPlayer player1;
    HeartsPlayer player2;

    PlayerPanel panelNorth ;
    PlayerPanel panelSouth;
    MainPanel panelCenter;
    
    public PockerTable( final HeartsDealer theDealer,final HeartsPlayer player1,final HeartsPlayer player2 ) {
        this.theDealer = theDealer;
        this.player1 = player1;
        this.player2 =player2;
        this.setSize(800, 600);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Hearts Table");
        this.setLayout(new BorderLayout());

        


//        JPanel panelEast = new JPanel();
        panelNorth = new PlayerPanel(player1);
        panelNorth.getButIntroduce().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                panelCenter.getArea().setText("--Player 1:\nHi! My name is " +
                        player1.getName() + " " +
                        player1.getSurname() + " and I'm " +
                        player1.getAge() + " years old!\n" +
                        "I love playing hearts and I'm known as " + player1.getNickname());
            }
        });
        panelNorth.getButShowHand().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JPanel hand = panelNorth.getPanelHand();
                //hand.removeAll();
                for (int i = 0; i < 5; i++) {
                    ((JLabel)hand.getComponent(i)).setIcon(new ImageIcon("Images\\cards\\"+player1.getHand()[i].toString()+".png"));
                }
                int hearts = player1.GetNumOfHearts();
                panelNorth.getLabHearts().setText("Number Of Hearts: "+hearts);
                panelCenter.getArea().setText("--Player 1:\nExw "+hearts+" koupes!");
            }
        });
        panelSouth = new PlayerPanel(player2);
        panelSouth.getButIntroduce().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                panelCenter.getArea().setText("--Player 2:\nHi! My name is " +
                        player2.getName() + " " +
                        player2.getSurname() + " and I'm " +
                        player2.getAge() + " years old!\n" +
                        "I love playing hearts and I'm known as " + player2.getNickname());
            }
        });
        panelSouth.getButShowHand().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JPanel hand = panelSouth.getPanelHand();
                //hand.removeAll();
                for (int i = 0; i < 5; i++) {
                    ((JLabel)hand.getComponent(i)).setIcon(new ImageIcon("Images\\cards\\"+player2.getHand()[i].toString()+".png"));
                }
                int hearts = player2.GetNumOfHearts();
                panelSouth.getLabHearts().setText("Number Of Hearts: "+hearts);
                panelCenter.getArea().setText("--Player 2:\nExw "+hearts+" koupes!");
            }
        });
        DealerPanel panelWest = new DealerPanel(theDealer);
        panelWest.getButIntruduce().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                panelCenter.getArea().setText("--Dealer:\nHi! I'm " + 
                        theDealer.getAge() + " and my name is " +
                        theDealer.getName() + " " +
                        theDealer.getSurname() + "\n" +
                        "I'm doing this job for " + theDealer.getYearsAsDealer() +
                        " years!");
            }
        });
        panelWest.getButShowDeck().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                panelCenter.getArea().setText("--Dealer:\nParakalw deite thn trapoula!");
                Card[] dc = theDealer.getDealersDeck().getDeckCards();
                for (int i = 0; i < dc.length; i++) {
                    panelCenter.getViewPanel().add(new EmptySpot("Images\\cards\\" + dc[i].toString() + ".png"));
                }
            }
        });
        panelWest.getButDeal().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                panelCenter.ClearViewPanel();
                panelCenter.repaint();
                theDealer.dealToPlayers(player1, player2);
                JPanel hand = panelNorth.getPanelHand();
                //hand.removeAll();
                for (int i = 0; i < 5; i++) {
                    ((JLabel)hand.getComponent(i)).setIcon(new ImageIcon("Images\\cards\\backview.png"));
                }
                hand = panelSouth.getPanelHand();
                //hand.removeAll();
                for (int i = 0; i < 5; i++) {
                    ((JLabel)hand.getComponent(i)).setIcon(new ImageIcon("Images\\cards\\backview.png"));
                }

                panelCenter.getArea().setText("--Dealer:\nMoirasa apo 5 fylla se kathe paixti. Parakalw deikste ta fylla sas.");
            }
        });
        panelWest.getButDecide().addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                int p1h = player1.GetNumOfHearts();
                int p2h = player2.GetNumOfHearts();
                String message;
                if (p1h > p2h) {
                    message = "--Dealer:\nO " + player1.getNickname()+
                            " kerdizei kai pairnei 20 pontous!\nKsekinaei neos gyros!!";
                    panelCenter.getArea().setText(message);
                    int oldp1points = Integer.parseInt(panelNorth.getLabPoints().getText().substring(14));
                    panelNorth.getLabPoints().setText("Total Points: "+(oldp1points + 20));
                }else if(p2h > p1h) {
                    message = "--Dealer:\nO " + player2.getNickname()+
                            " kerdizei kai pairnei 20 pontous!\nKsekinaei neos gyros!!";
                    panelCenter.getArea().setText(message);
                    int oldp2points = Integer.parseInt(panelSouth.getLabPoints().getText().substring(14));
                    panelSouth.getLabPoints().setText("Total Points: "+(oldp2points + 20));
                }else{
                    message = "--Dealer:\nIsopalia! Pairnete kai oi duo apo 10 pontous!\nKsekinaei neos gyros!!";
                    panelCenter.getArea().setText(message);
                    int oldp1points = Integer.parseInt(panelNorth.getLabPoints().getText().substring(14));
                    panelNorth.getLabPoints().setText("Total Points: "+(oldp1points + 10));
                    int oldp2points = Integer.parseInt(panelSouth.getLabPoints().getText().substring(14));
                    panelSouth.getLabPoints().setText("Total Points: "+(oldp2points + 10));
                }
                theDealer.getDealersDeck().Reset();
                JPanel hand = panelNorth.getPanelHand();
                //hand.removeAll();
                for (int i = 0; i < 5; i++) {
                    ((JLabel)hand.getComponent(i)).setIcon(new ImageIcon("Images\\cards\\empty.png"));
                }
                hand = panelSouth.getPanelHand();
                //hand.removeAll();
                for (int i = 0; i < 5; i++) {
                    ((JLabel)hand.getComponent(i)).setIcon(new ImageIcon("Images\\cards\\empty.png"));
                }
                panelSouth.getLabHearts().setText("Number Of Hearts: "+0);
                panelNorth.getLabHearts().setText("Number Of Hearts: "+0);
            }
        });

        panelCenter = new MainPanel();

        this.add(panelWest, BorderLayout.WEST);
        this.add(panelNorth, BorderLayout.NORTH);
        this.add(panelSouth, BorderLayout.SOUTH);
        this.add(panelCenter, BorderLayout.CENTER);


        this.setVisible(true);
    }
}
