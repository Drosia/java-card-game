/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ergasia3;

/**
 *
 * @author Gktistakis
 */
public class Deck {

    private Card[] deckCards = new Card[52];

    public void Reset()
    {
        deckCards = new Card[52];
         int p = 0;
        String sym, num;

        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    sym = "$";
                    break;
                case 1:
                    sym = "#";
                    break;
                case 2:
                    sym = "%";
                    break;
                case 3:
                    sym = "&";
                    break;
                default:
                    sym = "err";
                    break;
            }
            for (int j = 0; j < 13; j++) {
                switch (j) {
                    case 0:
                        num = "A";
                        break;
                    case 10:
                        num = "J";
                        break;
                    case 11:
                        num = "Q";
                        break;
                    case 12:
                        num = "K";
                        break;
                    default:
                        num = Integer.toString(j+1);
                        break;
                }
                deckCards[p] = new Card(sym, num);
                p++;
            }
        }
    }

    public Deck() {
        int p = 0;
        String sym, num;

        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    sym = "$";
                    break;
                case 1:
                    sym = "#";
                    break;
                case 2:
                    sym = "%";
                    break;
                case 3:
                    sym = "&";
                    break;
                default:
                    sym = "err";
                    break;
            }
            for (int j = 0; j < 13; j++) {
                switch (j) {
                    case 0:
                        num = "A";
                        break;
                    case 10:
                        num = "J";
                        break;
                    case 11:
                        num = "Q";
                        break;
                    case 12:
                        num = "K";
                        break;
                    default:
                        num = Integer.toString(j+1);
                        break;
                }
                deckCards[p] = new Card(sym, num);
                p++;
            }
        }

    }

    public Card[] getDeckCards() {
        return deckCards;
    }

}
