/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ergasia3;

/**
 *
 * @author ktistak
 */
public interface CardsDealer {  ////kathe klash pou kanei implement auto to interface tha prepei na exei tis parakatw sinartiseis ylopoihmenes,h na afinei tyxwn ypoklaseis tis na tis ylopoioun

    void showDeck();    //deixnei oli ta fylla tis trapoulas
    Card dealRandomCard();  //deinei mia tyxaia karta mesa apo tin trapoula
    void dealToPlayers(Object player1,Object player2);  //moirazei xartia se dyo paiktes
    void decideWinner(Object player1,Object player2);   //apofasizei poios nikise apo tous dyo paiktes

}
