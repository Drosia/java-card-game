/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ergasia3;

import javax.swing.*;
/**
 *
 * @author Gktistakis
 */
public class Card {
    private String symbol;
    private String number;
    private ImageIcon cardIcon;

    public Card() {
    }

    public Card(String symbol, String number) {
        this.symbol = symbol;
        this.number = number;
        this.cardIcon = new ImageIcon("Images/cards/"+symbol+number+".png");
//        System.out.println("Images/cards/"+symbol+number+".png");
    }

    public String getSymbol() {
        return symbol;
    }

    public String getNumber() {
        return number;
    }

    public ImageIcon getCardIcon() {
        return cardIcon;
    }

    public void setCardIcon(ImageIcon cardIcon) {
        this.cardIcon = cardIcon;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return this.symbol+this.number;
    }




}
