/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ergasia3;

import javax.swing.*;
/**
 *
 * @author Gktistakis
 */
public class EmptySpot extends JLabel{

    public EmptySpot() {
        this.setIcon(new ImageIcon("Images/cards/empty.png"));
    }

    public EmptySpot(String path) {
        this.setIcon(new ImageIcon(path));
    }

}
