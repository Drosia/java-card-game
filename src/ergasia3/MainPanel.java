/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ergasia3;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Gktistakis
 */
public class MainPanel extends JPanel {
    JTextArea area;
    JPanel viewPanel;

    public MainPanel() {
        area = new JTextArea();
        area.setEditable(false);
        
        this.setLayout(new GridLayout(2, 1));
        viewPanel = new JPanel();

        viewPanel.setPreferredSize(new Dimension(400, 1200));
        JScrollPane scrollPane = new JScrollPane(viewPanel);

        this.add(scrollPane);
        this.add(area);
        
    }

    public JTextArea getArea() {
        return area;
    }

    public JPanel getViewPanel() {
        return viewPanel;
    }

    public void ClearViewPanel(){
        viewPanel.removeAll();
    }

}
