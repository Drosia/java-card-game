/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ergasia3;

/**
 *
 * @author ktistak
 */
public abstract class Human {   //einai abstract,ara den mporei na dimiourgithei instance tis

    private String name;    //kathe anthrwpos exei opwsdipote onoma,epitheto kai hlikia
    private String surname;
    private int age;

//    constructor
    public Human() {
        this.name = null;
        this.surname = null;
        this.age = 1;
    }

    public Human(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

//    getters
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

//    setters

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public abstract void introduceSelf();

}
